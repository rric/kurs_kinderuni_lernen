setlocal 

set filename=Richter+Rollak_Bericht-KinderUni-2015

del "%filename%.docx"

pandoc -f markdown --standalone -o "%filename%.docx" "%filename%.txt"

:: Without the "Title" this just opens a CMD window:
start "Title" "%filename%.docx"

endlocal
